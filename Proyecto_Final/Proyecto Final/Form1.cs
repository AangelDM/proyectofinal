﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_Final
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCambio_Click(object sender, EventArgs e)
        {
            this.Hide();
            Ventas VentanaVentas = new Ventas();
            VentanaVentas.Show();
        }

        private void buttonInv_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventario VentanaInv = new Inventario();
            VentanaInv.Show();
        }
    }
}
