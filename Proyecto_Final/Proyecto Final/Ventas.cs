﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;
namespace Proyecto_Final
{
    public partial class Ventas : Form
    {
        DataTable dt;
        int sum = 0;
        int tot =0;
        public Ventas()
        {
            InitializeComponent();
            dt = new DataTable();

            dt.Columns.Add("Codigo");
            dt.Columns.Add("Nombre");
            dt.Columns.Add("Cantidad");
            dt.Columns.Add("Precio");
            dataGridView1.DataSource = dt;

        }

        private void buttonAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 Inicio = new Form1();
            Inicio.Show();
        }

   
        private void labelnombrev_Click(object sender, EventArgs e)
        {

        }

        private void productos_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonlista_Click_1(object sender, EventArgs e)
        {
            using (StreamReader lector = new StreamReader(@"C:\Users\Public\MyTest.txt"))
            {

                string s = lector.ReadLine();
                string[] datos = new string[4];

                while (s != null)
                {
                    string[] arr = s.Split(",");
                    datos[0] = arr[0];
                    datos[1] = arr[1];
                    datos[2] = arr[2];
                    datos[3] = arr[3];
                    DataRow row = dt.NewRow();
                    row["Codigo"] = datos[0];
                    row["Nombre"] = datos[1];
                    row["Cantidad"] = datos[2];
                    row["Precio"] = datos[3];
                    dt.Rows.Add(row);
                    s = lector.ReadLine();
                }
            }
        }

        private void buttonagregar_Click(object sender, EventArgs e)
        {
            sum = int.Parse(textprecio.Text);
            tot = tot + sum;
            String codigo = textcodigo.Text;
            String nombre = textnombre.Text;
            String precio = textprecio.Text;
            StreamWriter escribir = File.AppendText(@"C:\Users\Public\MyTicket.txt");
            escribir.WriteLine(" "+codigo+" , "+nombre+" , "+precio);
            escribir.Close();
            textTicket.Multiline = true;
            textTicket.AppendText(codigo + " , " + nombre + " , " + precio + "\r\n");

            int filita = int.Parse(textcodigo.Text)-1;
            int valor = int.Parse(String.Concat(dataGridView1.Rows[filita].Cells[2].Value));
            dataGridView1.Rows[filita].Cells[2].Value = valor - 1;
        }

        private void buttonfinalizar_Click(object sender, EventArgs e)
        {
            textcodigo.Clear();
            textnombre.Clear();
            textprecio.Clear();
            MessageBox.Show("Compra finalizada\n" + "Total a pagar: " + tot);

            File.Delete(@"C:\Users\Public\MyTest.txt");

            string[] codigos = new string[dt.Rows.Count];
            string[] nombres = new string[dt.Rows.Count];
            string[] cantidades = new string[dt.Rows.Count];
            string[] precios = new string[dt.Rows.Count];

            for (int p = 0; p < dt.Rows.Count; p++)
            {
                codigos[p] = String.Concat(dataGridView1.Rows[p].Cells[0].Value);
                nombres[p] = String.Concat(dataGridView1.Rows[p].Cells[1].Value);
                cantidades[p] = String.Concat(dataGridView1.Rows[p].Cells[2].Value);
                precios[p] = String.Concat(dataGridView1.Rows[p].Cells[3].Value);
            }

            StreamWriter escribir = File.AppendText(@"C:\Users\Public\MyTest.txt");
            for (int o = 0; o < dt.Rows.Count; o++)
            {
                escribir.WriteLine(codigos[o] + "," + nombres[o] + "," + cantidades[o] + "," + precios[o]);
            }
            escribir.Close();

            dt.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textTicket.Clear();

            string path = @"C:\Users\Public\MyTicket.txt";
            try
            {
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(path))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes("");
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
                // Open the stream and read it back.
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                   
                }
                using (StreamReader lector = new StreamReader(@"C:\Users\Public\MyTest.txt"))
                {

                    string s = lector.ReadLine();
                    string[] datos = new string[4];

                    while (s != null)
                    {
                        string[] arr = s.Split(",");
                        datos[0] = arr[0];
                        datos[1] = arr[1];
                        datos[2] = arr[2];
                        datos[3] = arr[3];
                        DataRow row = dt.NewRow();
                        row["Codigo"] = datos[0];
                        row["Nombre"] = datos[1];
                        row["Cantidad"] = datos[2];
                        row["Precio"] = datos[3];
                        dt.Rows.Add(row);
                        s = lector.ReadLine();
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void textnombrev_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int valor = dataGridView1.CurrentRow.Index;
            textcodigo.Text = String.Concat(dataGridView1.Rows[valor].Cells[0].Value);
            textnombre.Text = String.Concat(dataGridView1.Rows[valor].Cells[1].Value);
            textprecio.Text = String.Concat(dataGridView1.Rows[valor].Cells[3].Value);

        }
    }
}
