﻿
namespace Proyecto_Final
{
    partial class Ventas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ventas));
            this.buttonAtras = new System.Windows.Forms.Button();
            this.textcodigo = new System.Windows.Forms.TextBox();
            this.buttonagregar = new System.Windows.Forms.Button();
            this.buttonfinalizar = new System.Windows.Forms.Button();
            this.labelticket = new System.Windows.Forms.Label();
            this.labelventas = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonticket = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textnombre = new System.Windows.Forms.TextBox();
            this.textprecio = new System.Windows.Forms.TextBox();
            this.textTicket = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAtras
            // 
            this.buttonAtras.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonAtras.Location = new System.Drawing.Point(12, 12);
            this.buttonAtras.Name = "buttonAtras";
            this.buttonAtras.Size = new System.Drawing.Size(133, 48);
            this.buttonAtras.TabIndex = 0;
            this.buttonAtras.Text = "Atras";
            this.buttonAtras.UseVisualStyleBackColor = true;
            this.buttonAtras.Click += new System.EventHandler(this.buttonAtras_Click);
            // 
            // textcodigo
            // 
            this.textcodigo.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textcodigo.Location = new System.Drawing.Point(565, 225);
            this.textcodigo.Margin = new System.Windows.Forms.Padding(2);
            this.textcodigo.Name = "textcodigo";
            this.textcodigo.Size = new System.Drawing.Size(150, 32);
            this.textcodigo.TabIndex = 3;
            this.textcodigo.TextChanged += new System.EventHandler(this.textnombrev_TextChanged);
            // 
            // buttonagregar
            // 
            this.buttonagregar.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonagregar.Location = new System.Drawing.Point(517, 449);
            this.buttonagregar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonagregar.Name = "buttonagregar";
            this.buttonagregar.Size = new System.Drawing.Size(240, 40);
            this.buttonagregar.TabIndex = 5;
            this.buttonagregar.Text = "Agregar al carrito";
            this.buttonagregar.UseVisualStyleBackColor = true;
            this.buttonagregar.Click += new System.EventHandler(this.buttonagregar_Click);
            // 
            // buttonfinalizar
            // 
            this.buttonfinalizar.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonfinalizar.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonfinalizar.Location = new System.Drawing.Point(517, 505);
            this.buttonfinalizar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonfinalizar.Name = "buttonfinalizar";
            this.buttonfinalizar.Size = new System.Drawing.Size(240, 43);
            this.buttonfinalizar.TabIndex = 6;
            this.buttonfinalizar.Text = "Finalizar compra";
            this.buttonfinalizar.UseVisualStyleBackColor = false;
            this.buttonfinalizar.Click += new System.EventHandler(this.buttonfinalizar_Click);
            // 
            // labelticket
            // 
            this.labelticket.AutoSize = true;
            this.labelticket.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelticket.Location = new System.Drawing.Point(228, 394);
            this.labelticket.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelticket.Name = "labelticket";
            this.labelticket.Size = new System.Drawing.Size(74, 33);
            this.labelticket.TabIndex = 9;
            this.labelticket.Text = "Ticket";
            // 
            // labelventas
            // 
            this.labelventas.AutoSize = true;
            this.labelventas.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelventas.Location = new System.Drawing.Point(347, 22);
            this.labelventas.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelventas.Name = "labelventas";
            this.labelventas.Size = new System.Drawing.Size(99, 43);
            this.labelventas.TabIndex = 10;
            this.labelventas.Text = "Ventas";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Proyecto_Final.Properties.Resources.bolsa;
            this.pictureBox1.Location = new System.Drawing.Point(565, 12);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(153, 183);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // buttonticket
            // 
            this.buttonticket.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonticket.Location = new System.Drawing.Point(517, 394);
            this.buttonticket.Margin = new System.Windows.Forms.Padding(2);
            this.buttonticket.Name = "buttonticket";
            this.buttonticket.Size = new System.Drawing.Size(240, 37);
            this.buttonticket.TabIndex = 14;
            this.buttonticket.Text = "Iniciar ticket";
            this.buttonticket.UseVisualStyleBackColor = true;
            this.buttonticket.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(46, 108);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(437, 269);
            this.dataGridView1.TabIndex = 15;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // textnombre
            // 
            this.textnombre.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textnombre.Location = new System.Drawing.Point(565, 271);
            this.textnombre.Margin = new System.Windows.Forms.Padding(2);
            this.textnombre.Name = "textnombre";
            this.textnombre.Size = new System.Drawing.Size(150, 32);
            this.textnombre.TabIndex = 16;
            // 
            // textprecio
            // 
            this.textprecio.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textprecio.Location = new System.Drawing.Point(565, 319);
            this.textprecio.Margin = new System.Windows.Forms.Padding(2);
            this.textprecio.Name = "textprecio";
            this.textprecio.Size = new System.Drawing.Size(150, 32);
            this.textprecio.TabIndex = 17;
            // 
            // textTicket
            // 
            this.textTicket.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textTicket.Location = new System.Drawing.Point(147, 430);
            this.textTicket.Multiline = true;
            this.textTicket.Name = "textTicket";
            this.textTicket.Size = new System.Drawing.Size(246, 138);
            this.textTicket.TabIndex = 18;
            // 
            // Ventas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(793, 600);
            this.Controls.Add(this.textTicket);
            this.Controls.Add(this.textprecio);
            this.Controls.Add(this.textnombre);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonticket);
            this.Controls.Add(this.buttonfinalizar);
            this.Controls.Add(this.buttonagregar);
            this.Controls.Add(this.textcodigo);
            this.Controls.Add(this.labelventas);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelticket);
            this.Controls.Add(this.buttonAtras);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Ventas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ventas";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAtras;
        private System.Windows.Forms.TextBox textcodigo;
        private System.Windows.Forms.Button buttonagregar;
        private System.Windows.Forms.Button buttonfinalizar;
        private System.Windows.Forms.Label labelticket;
        private System.Windows.Forms.Label labelventas;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonticket;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textnombre;
        private System.Windows.Forms.TextBox textprecio;
        private System.Windows.Forms.TextBox textTicket;
    }
}