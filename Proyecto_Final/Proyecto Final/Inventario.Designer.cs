﻿
namespace Proyecto_Final
{
    partial class Inventario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inventario));
            this.labelcodigo = new System.Windows.Forms.Label();
            this.labelprecio = new System.Windows.Forms.Label();
            this.textCodigo = new System.Windows.Forms.TextBox();
            this.textprecio = new System.Windows.Forms.TextBox();
            this.labelinventario = new System.Windows.Forms.Label();
            this.buttonagregar = new System.Windows.Forms.Button();
            this.labellista = new System.Windows.Forms.Label();
            this.buttonregreso = new System.Windows.Forms.Button();
            this.buttonelim = new System.Windows.Forms.Button();
            this.buttonborrartodo = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBoxCantidad = new System.Windows.Forms.TextBox();
            this.textBoxNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonModificar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelcodigo
            // 
            this.labelcodigo.AutoSize = true;
            this.labelcodigo.BackColor = System.Drawing.Color.Transparent;
            this.labelcodigo.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelcodigo.Location = new System.Drawing.Point(514, 137);
            this.labelcodigo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelcodigo.Name = "labelcodigo";
            this.labelcodigo.Size = new System.Drawing.Size(80, 33);
            this.labelcodigo.TabIndex = 0;
            this.labelcodigo.Text = "Codigo";
            // 
            // labelprecio
            // 
            this.labelprecio.AutoSize = true;
            this.labelprecio.BackColor = System.Drawing.Color.Transparent;
            this.labelprecio.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelprecio.Location = new System.Drawing.Point(519, 308);
            this.labelprecio.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelprecio.Name = "labelprecio";
            this.labelprecio.Size = new System.Drawing.Size(75, 33);
            this.labelprecio.TabIndex = 2;
            this.labelprecio.Text = "Precio";
            // 
            // textCodigo
            // 
            this.textCodigo.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textCodigo.Location = new System.Drawing.Point(630, 137);
            this.textCodigo.Margin = new System.Windows.Forms.Padding(2);
            this.textCodigo.Name = "textCodigo";
            this.textCodigo.Size = new System.Drawing.Size(176, 32);
            this.textCodigo.TabIndex = 3;
            // 
            // textprecio
            // 
            this.textprecio.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textprecio.Location = new System.Drawing.Point(630, 308);
            this.textprecio.Margin = new System.Windows.Forms.Padding(2);
            this.textprecio.Name = "textprecio";
            this.textprecio.Size = new System.Drawing.Size(176, 32);
            this.textprecio.TabIndex = 5;
            this.textprecio.TextChanged += new System.EventHandler(this.textprecio_TextChanged);
            // 
            // labelinventario
            // 
            this.labelinventario.AutoSize = true;
            this.labelinventario.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelinventario.Location = new System.Drawing.Point(309, 9);
            this.labelinventario.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelinventario.Name = "labelinventario";
            this.labelinventario.Size = new System.Drawing.Size(177, 43);
            this.labelinventario.TabIndex = 6;
            this.labelinventario.Text = "INVENTARIO";
            // 
            // buttonagregar
            // 
            this.buttonagregar.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonagregar.Location = new System.Drawing.Point(18, 511);
            this.buttonagregar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonagregar.Name = "buttonagregar";
            this.buttonagregar.Size = new System.Drawing.Size(123, 47);
            this.buttonagregar.TabIndex = 7;
            this.buttonagregar.Text = "Agregar";
            this.buttonagregar.UseVisualStyleBackColor = true;
            this.buttonagregar.Click += new System.EventHandler(this.buttonagregar_Click);
            // 
            // labellista
            // 
            this.labellista.AutoSize = true;
            this.labellista.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labellista.Location = new System.Drawing.Point(202, 74);
            this.labellista.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labellista.Name = "labellista";
            this.labellista.Size = new System.Drawing.Size(61, 33);
            this.labellista.TabIndex = 10;
            this.labellista.Text = "Lista";
            this.labellista.Click += new System.EventHandler(this.label2_Click);
            // 
            // buttonregreso
            // 
            this.buttonregreso.Font = new System.Drawing.Font("Segoe Print", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonregreso.Location = new System.Drawing.Point(18, 8);
            this.buttonregreso.Margin = new System.Windows.Forms.Padding(2);
            this.buttonregreso.Name = "buttonregreso";
            this.buttonregreso.Size = new System.Drawing.Size(78, 31);
            this.buttonregreso.TabIndex = 11;
            this.buttonregreso.Text = "Regresar";
            this.buttonregreso.UseVisualStyleBackColor = true;
            this.buttonregreso.Click += new System.EventHandler(this.buttonregreso_Click);
            // 
            // buttonelim
            // 
            this.buttonelim.BackColor = System.Drawing.Color.IndianRed;
            this.buttonelim.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonelim.Location = new System.Drawing.Point(181, 511);
            this.buttonelim.Margin = new System.Windows.Forms.Padding(2);
            this.buttonelim.Name = "buttonelim";
            this.buttonelim.Size = new System.Drawing.Size(123, 47);
            this.buttonelim.TabIndex = 12;
            this.buttonelim.Text = "Eliminar";
            this.buttonelim.UseVisualStyleBackColor = false;
            this.buttonelim.Click += new System.EventHandler(this.buttonelim_Click);
            // 
            // buttonborrartodo
            // 
            this.buttonborrartodo.BackColor = System.Drawing.Color.Tomato;
            this.buttonborrartodo.Location = new System.Drawing.Point(675, 574);
            this.buttonborrartodo.Margin = new System.Windows.Forms.Padding(2);
            this.buttonborrartodo.Name = "buttonborrartodo";
            this.buttonborrartodo.Size = new System.Drawing.Size(136, 23);
            this.buttonborrartodo.TabIndex = 13;
            this.buttonborrartodo.Text = "Borrar base de datos";
            this.buttonborrartodo.UseVisualStyleBackColor = false;
            this.buttonborrartodo.Click += new System.EventHandler(this.buttonborrartodo_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Proyecto_Final.Properties.Resources.cajas;
            this.pictureBox1.Location = new System.Drawing.Point(545, 355);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(261, 232);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(675, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 49);
            this.button1.TabIndex = 17;
            this.button1.Text = "Cargar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(18, 125);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(443, 345);
            this.dataGridView1.TabIndex = 18;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // textBoxCantidad
            // 
            this.textBoxCantidad.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxCantidad.Location = new System.Drawing.Point(630, 255);
            this.textBoxCantidad.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxCantidad.Name = "textBoxCantidad";
            this.textBoxCantidad.Size = new System.Drawing.Size(176, 32);
            this.textBoxCantidad.TabIndex = 19;
            // 
            // textBoxNombre
            // 
            this.textBoxNombre.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxNombre.Location = new System.Drawing.Point(630, 199);
            this.textBoxNombre.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNombre.Name = "textBoxNombre";
            this.textBoxNombre.Size = new System.Drawing.Size(176, 32);
            this.textBoxNombre.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(514, 198);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 33);
            this.label1.TabIndex = 21;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(514, 255);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 33);
            this.label2.TabIndex = 22;
            this.label2.Text = "Cantidad";
            // 
            // buttonModificar
            // 
            this.buttonModificar.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonModificar.Location = new System.Drawing.Point(338, 511);
            this.buttonModificar.Margin = new System.Windows.Forms.Padding(2);
            this.buttonModificar.Name = "buttonModificar";
            this.buttonModificar.Size = new System.Drawing.Size(123, 47);
            this.buttonModificar.TabIndex = 23;
            this.buttonModificar.Text = "Modificar";
            this.buttonModificar.UseVisualStyleBackColor = true;
            this.buttonModificar.Click += new System.EventHandler(this.buttonModificar_Click);
            // 
            // Inventario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(822, 598);
            this.Controls.Add(this.buttonborrartodo);
            this.Controls.Add(this.buttonModificar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxNombre);
            this.Controls.Add(this.textBoxCantidad);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textCodigo);
            this.Controls.Add(this.textprecio);
            this.Controls.Add(this.labelprecio);
            this.Controls.Add(this.labelcodigo);
            this.Controls.Add(this.labelinventario);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonelim);
            this.Controls.Add(this.buttonregreso);
            this.Controls.Add(this.labellista);
            this.Controls.Add(this.buttonagregar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Inventario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inventario";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelcodigo;
        private System.Windows.Forms.Label labelprecio;
        private System.Windows.Forms.TextBox textCodigo;
        private System.Windows.Forms.TextBox textprecio;
        private System.Windows.Forms.Label labelinventario;
        private System.Windows.Forms.Button buttonagregar;
        private System.Windows.Forms.Label labellista;
        private System.Windows.Forms.Button buttonregreso;
        private System.Windows.Forms.Button buttonelim;
        private System.Windows.Forms.Button buttonborrartodo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBoxCantidad;
        private System.Windows.Forms.TextBox textBoxNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonModificar;
    }
}