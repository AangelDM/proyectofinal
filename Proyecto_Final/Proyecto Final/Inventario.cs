﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace Proyecto_Final
{
    public partial class Inventario : Form
    {
        DataTable dt;
        public Inventario()
        {
            InitializeComponent();
            dt = new DataTable();
            dt.Columns.Add("Codigo");
            dt.Columns.Add("Nombre");
            dt.Columns.Add("Cantidad");
            dt.Columns.Add("Precio");

            dataGridView1.DataSource = dt;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void buttonregreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 ventana = new Form1();
            ventana.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string path = @"C:\Users\Public\MyTest.txt";
            try
            {
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(path))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes("**************");
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }

                // Open the stream and read it back.
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        MessageBox.Show(s);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void buttonagregar_Click(object sender, EventArgs e)
        {
            String codigo = textCodigo.Text;
            String nombre = textBoxNombre.Text;
            String cantidad = textBoxCantidad.Text;
            String precio = textprecio.Text;

            DataRow row = dt.NewRow();
            row["Codigo"] = codigo;
            row["Nombre"] = nombre;
            row["Cantidad"] = cantidad;
            row["Precio"] = precio;

            dt.Rows.Add(row);

            StreamWriter escribir = File.AppendText(@"C:\Users\Public\MyTest.txt");
            escribir.WriteLine(codigo + "," + nombre + ","+cantidad+","+precio);
            escribir.Close();

            textCodigo.Clear();
            textBoxNombre.Clear();
            textBoxCantidad.Clear();
            textprecio.Clear();

        }

        private void buttonborrartodo_Click(object sender, EventArgs e)
        {
            dt.Clear();
            string ruta = @"C:\Users\Public\MyTest.txt";
            try
            {
                File.Delete(ruta);
                if (File.Exists(ruta))
                {
                    Console.WriteLine("El archivo sigue existiendo.");
                }
                else
                {
                    Console.WriteLine("El archivo ya no existe.");
                    var resul = MessageBox.Show("Se elimino la base de datos", "Inventario", MessageBoxButtons.OK);

                }
            }
            catch (Exception f)
            {
                Console.WriteLine("Error al borrar archivo: {0}", f.ToString());
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonelim_Click(object sender, EventArgs e)
        {
            int valor = 0;
            for (int i = dt.Rows.Count-1; i >= 0; i--)
            {
                DataRow fila = dt.Rows[i];
                foreach (DataRow filas in dt.Rows)
                {
                    valor = int.Parse(String.Concat(filas["Codigo"]));

                }
                if (valor == int.Parse(textCodigo.Text))
                {
                    fila.Delete();

                    string[] codigos = new string[dt.Rows.Count];
                    string[] nombres = new string[dt.Rows.Count];
                    string[] cantidades = new string[dt.Rows.Count];
                    string[] precios = new string[dt.Rows.Count];

                    for(int p = 0; p < dt.Rows.Count; p++)
                    {
                        codigos[p] = String.Concat(dataGridView1.Rows[p].Cells[0].Value);
                        nombres[p] = String.Concat(dataGridView1.Rows[p].Cells[1].Value);
                        cantidades[p] = String.Concat(dataGridView1.Rows[p].Cells[2].Value);
                        precios[p] = String.Concat(dataGridView1.Rows[p].Cells[3].Value);
                    }

                    File.Delete(@"C:\Users\Public\MyTest.txt");

                    StreamWriter escribir = File.AppendText(@"C:\Users\Public\MyTest.txt");
                    for(int o = 0; o < dt.Rows.Count; o++)
                    {
                        escribir.WriteLine(codigos[o] + "," + nombres[o] + "," + cantidades[o] + "," + precios[o]);
                    }
                    escribir.Close();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (StreamReader lector = new StreamReader(@"C:\Users\Public\MyTest.txt"))
            {
                
                string s = lector.ReadLine();
                string[] datos = new string[4];
                
                while (s != null)
                {
                    string[] arr = s.Split(",");
                    datos[0] = arr[0];
                    datos[1] = arr[1];
                    datos[2] = arr[2];
                    datos[3] = arr[3];
                    DataRow row = dt.NewRow();
                    row["Codigo"] = datos[0];
                    row["Nombre"] = datos[1];
                    row["Cantidad"] = datos[2];
                    row["Precio"] = datos[3];
                    dt.Rows.Add(row);
                    s = lector.ReadLine();
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textprecio_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int valor = dataGridView1.CurrentRow.Index;
            textCodigo.Text = String.Concat(dataGridView1.Rows[valor].Cells[0].Value);
            textBoxNombre.Text = String.Concat(dataGridView1.Rows[valor].Cells[1].Value);
            textBoxCantidad.Text = String.Concat(dataGridView1.Rows[valor].Cells[2].Value);
            textprecio.Text = String.Concat(dataGridView1.Rows[valor].Cells[3].Value);
        }

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            int valor = 0;
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow fila = dt.Rows[i];
                foreach (DataRow filas in dt.Rows)
                {
                    valor = int.Parse(String.Concat(filas["Codigo"]));

                }
                if (valor == int.Parse(textCodigo.Text))
                {
                    int filita = valor - 1;
                    dataGridView1.Rows[filita].Cells[1].Value = textBoxNombre.Text;
                    dataGridView1.Rows[filita].Cells[2].Value = textBoxCantidad.Text;
                    dataGridView1.Rows[filita].Cells[3].Value = textprecio.Text;

                    string[] codigos = new string[dt.Rows.Count];
                    string[] nombres = new string[dt.Rows.Count];
                    string[] cantidades = new string[dt.Rows.Count];
                    string[] precios = new string[dt.Rows.Count];

                    for (int p = 0; p < dt.Rows.Count; p++)
                    {
                        codigos[p] = String.Concat(dataGridView1.Rows[p].Cells[0].Value);
                        nombres[p] = String.Concat(dataGridView1.Rows[p].Cells[1].Value);
                        cantidades[p] = String.Concat(dataGridView1.Rows[p].Cells[2].Value);
                        precios[p] = String.Concat(dataGridView1.Rows[p].Cells[3].Value);
                    }

                    File.Delete(@"C:\Users\Public\MyTest.txt");

                    StreamWriter escribir = File.AppendText(@"C:\Users\Public\MyTest.txt");
                    for (int o = 0; o < dt.Rows.Count; o++)
                    {
                        escribir.WriteLine(codigos[o] + "," + nombres[o] + "," + cantidades[o] + "," + precios[o]);
                    }
                    escribir.Close();
                }
            }
        }
    }
}
